package SantiagoBrozt.Modelo;

import SantiagoBrozt.Persistencia.LectorDeEmpleadosCSV;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.MonthDay;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FelizCumpleTest {
    @Test
    public void felicitacion() {
        var empleados = new LectorDeEmpleados() {
            @Override
            public List<Empleado> recuperarEmpleados() throws RuntimeException {
                return List.of(
                        new Empleado("Brozt", "Santiago",
                                LocalDate.of(2024, 7,31),
                                "santiagobrozt@gmail.com"),
                        new Empleado("Rodríguez", "Lautaro",
                                LocalDate.of(2024, 4, 28),
                                "lautarorodriguez@yahoo.com"));
            }
        };

        var comunicador = new Comunicador() {
            private List<String> cumplanierosFelicitados = new ArrayList<>();
            @Override
            public void comunicar(String mensaje, String email) {
                cumplanierosFelicitados.add(email);
            }

            public boolean fueFelicitado(String cumplaniero) {
                return this.cumplanierosFelicitados.contains(cumplaniero);
            }
        };

        var felicitador = new FelicitadorDeCumple(empleados, comunicador);
        felicitador.felicitar(MonthDay.of(4, 28));
        assertTrue(comunicador.fueFelicitado("lautarorodriguez@yahoo.com"));
        assertFalse(comunicador.fueFelicitado("santiagobrozt@gmail.com"));
    }
}
