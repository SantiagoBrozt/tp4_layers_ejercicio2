package SantiagoBrozt.Main;

import SantiagoBrozt.Comunicacion.PorMailComunicar;
import SantiagoBrozt.Modelo.FelicitadorDeCumple;
import SantiagoBrozt.Persistencia.LectorDeEmpleadosCSV;

import java.time.MonthDay;

public class Main {
    public static void main(String[] args) {
        new FelicitadorDeCumple(
                new LectorDeEmpleadosCSV("src/main/resources/empleados.csv"),
                new PorMailComunicar()).felicitar(MonthDay.now());
    }
}
