package SantiagoBrozt.Persistencia;

import SantiagoBrozt.Modelo.Empleado;
import SantiagoBrozt.Modelo.LectorDeEmpleados;
import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class LectorDeEmpleadosCSV implements LectorDeEmpleados {
    private String localPath;

    public LectorDeEmpleadosCSV(String path) {
        this.localPath = path;
    }

    public List<Empleado> recuperarEmpleados() throws RuntimeException {
        var empleados = new ArrayList<Empleado>();
        var datos = leerCSV();
        for (String[] fila : datos) {
            empleados.add(new Empleado(fila[0], fila[1], LocalDate.parse(fila[2]), fila[3]));
        }
        return empleados;
    }

    private List<String[]> leerCSV() throws RuntimeException {
        List<String[]> datos = new ArrayList<String[]>();
        try {
            CSVReader reader = new CSVReader(new FileReader(localPath));
            String[] row = null;
            while ((row = reader.readNext()) != null) {
                datos.add(row);
            }
            reader.close();
            datos.remove(0);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        return datos;
    }
}
