package SantiagoBrozt.Modelo;

import java.util.List;

public interface LectorDeEmpleados {
    public List<Empleado> recuperarEmpleados() throws RuntimeException;
}
