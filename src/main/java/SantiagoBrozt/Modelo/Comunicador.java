package SantiagoBrozt.Modelo;

public interface Comunicador {
    public void comunicar(String mensaje, String email);
}
