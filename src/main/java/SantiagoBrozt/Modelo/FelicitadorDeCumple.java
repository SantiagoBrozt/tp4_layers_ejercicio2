package SantiagoBrozt.Modelo;

import java.time.LocalDate;
import java.time.MonthDay;
import java.util.List;

public class FelicitadorDeCumple {
    private List<Empleado> empleados;
    private LectorDeEmpleados lectorDeEmpleados;
    private Comunicador comunicador;

    public FelicitadorDeCumple(LectorDeEmpleados lectorDeArchivo, Comunicador comunicador) {
        this.lectorDeEmpleados = lectorDeArchivo;
        this.comunicador = comunicador;
    }

    public void felicitar(MonthDay fecha) {
        var empleados = lectorDeEmpleados.recuperarEmpleados();
        for (Empleado empleado : empleados) {
                if (empleado.cumpleAniosHoy(fecha)) {
                   comunicador.comunicar("Feliz Cumpleaños " + empleado.nombre() + " "
                           + empleado.apellido(), empleado.email());
            }
        }
    }
}
