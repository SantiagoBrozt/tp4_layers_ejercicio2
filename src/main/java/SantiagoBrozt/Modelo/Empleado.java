package SantiagoBrozt.Modelo;

import java.time.LocalDate;
import java.time.MonthDay;
import java.util.function.Supplier;

public class Empleado {
    private String apellido;
    private String nombre;
    private LocalDate fechaNac;
    private String email;
    private Supplier<LocalDate> proveedorDeFecha;

    public Empleado(String apellido, String nombre, LocalDate fechaNac, String email) {
        this.apellido = apellido;
        this.nombre = nombre;
        this.fechaNac = fechaNac;
        this.email = email;
    }

    public boolean cumpleAniosHoy(MonthDay fecha) {
        return fechaNac.equals(LocalDate.of(fechaNac.getYear(), fecha.getMonth(), fecha.getDayOfMonth()));
    }

    public String apellido() {
        return this.apellido;
    }

    public String nombre() {
        return this.nombre;
    }

    public String email() {
        return this.email;
    }
}
